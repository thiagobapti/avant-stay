import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import "./button.scss";

const block = "button";
const themes = ["green-outline", "dark-outline", "link", "solid-green"];
const Button = React.forwardRef(
  ({ theme, children, className, ...rest }, ref) => {
    return (
      <button
        ref={ref}
        className={cn(block, `${block}--${theme}`, className)}
        {...rest}
      >
        {children}
      </button>
    );
  }
);

Button.propTypes = {
  theme: PropTypes.oneOf(themes),
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  className: PropTypes.string,
};

Button.defaultProps = {
  theme: themes[0],
};

export default Button;
