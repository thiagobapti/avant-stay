import React, {
  useContext,
  useCallback,
  useEffect,
  useState,
  useMemo,
} from "react";
import "./main-header.scss";
import {
  fetchProperties,
  fetchHomesPricing,
} from "../../services/property-service";
import { AppContext } from "../../App";
import chevronDownBlueIcon from "../../images/chevron-down-blue-icon.svg";
import { Link } from "react-router-dom";
import Button from "../button/button";
import cn from "classnames";
import { fetchRegions } from "../../services/region-service";
import { useSearchParams } from "react-router-dom";

const block = "main-header";
const MainHeader = () => {
  const {
    setFilterContext,
    regionContext,
    filterContext,
    propertiesContext,
    setRegionContext,
    setPropertiesContext,
  } = useContext(AppContext);
  const [searchParams, setSearchParams] = useSearchParams();
  const [isOpen, setIsOpen] = useState(false);
  const [where, setWhere] = useState(searchParams.get("where") || "");
  const [when, setWhen] = useState(searchParams.get("when") || "");
  const [who, setWho] = useState(searchParams.get("who") || "");
  const [sort, setSort] = useState(searchParams.get("sort") || "RELEVANCE");
  const [coupon, setCoupon] = useState(searchParams.get("coupon") || "");

  const queryStringParams = useMemo(() => {
    const params = {};
    searchParams.forEach((value, key) => {
      params[key] = value;
    });
    return params;
  }, [searchParams]);

  const guestsOptions = useMemo(() => {
    const options = [];
    for (let optionIndex = 1; optionIndex <= 30; optionIndex++) {
      options.push(
        <option key={optionIndex} value={optionIndex}>
          {optionIndex}
        </option>
      );
    }
    return options;
  }, []);

  const whereChangeHandler = useCallback(
    (event) => {
      const { value } = event?.target;
      const { where, ...rest } = queryStringParams;

      setWhere(value);
      setSearchParams(value ? { ...queryStringParams, where: value } : rest);
    },
    [setSearchParams, queryStringParams]
  );

  const whenChangeHandler = useCallback(
    (event) => {
      const { value } = event?.target;
      const { when, ...rest } = queryStringParams;

      setWhen(value);
      setSearchParams(value ? { ...queryStringParams, when: value } : rest);
    },
    [setSearchParams, queryStringParams]
  );

  const whoChangeHandler = useCallback(
    (event) => {
      const { value } = event?.target;
      const { who, ...rest } = queryStringParams;

      setWho(value);
      setSearchParams(value ? { ...queryStringParams, who: value } : rest);
    },
    [setSearchParams, queryStringParams]
  );

  const sortChangeHandler = useCallback(
    (event) => {
      const { value } = event?.target;
      const { sort, ...rest } = queryStringParams;

      setSort(value);
      setSearchParams(
        value !== "relevance" ? { ...queryStringParams, sort: value } : rest
      );
    },
    [setSearchParams, queryStringParams]
  );

  const handlePricesUpdate = useCallback((prices, results) => {
    prices.forEach((price) => {
      results.forEach((property) => {
        if (price.homeId === property.id) {
          property.pricing = price;
        }
      });
    });

    return results;
  }, []);

  const getPrices = useCallback(
    (ids, checkIn, checkOut, results, context) => {
      fetchHomesPricing(ids, checkIn, checkOut).then((response) => {
        handlePricesUpdate(response, results);
        setPropertiesContext({ ...context });
      });
    },
    [handlePricesUpdate, setPropertiesContext]
  );

  const couponChangeHandler = useCallback(
    (event) => {
      const { value } = event?.target;
      const { coupon, ...rest } = queryStringParams;

      setCoupon(value);
      setSearchParams(value ? { ...queryStringParams, coupon: value } : rest);

      if (filterContext.checkIn && filterContext.checkOut) {
        const propertiesResults = propertiesContext?.results?.map(
          (property) => {
            const { pricing, ...rest } = property;
            return rest;
          }
        );
        setPropertiesContext({
          ...propertiesContext,
          results: propertiesResults,
        });
        const ids = propertiesContext?.results?.map((property) => property.id);
        getPrices(
          ids,
          filterContext.checkIn,
          filterContext.checkOut,
          propertiesResults,
          propertiesContext
        );
      }
    },
    [
      setSearchParams,
      queryStringParams,
      propertiesContext,
      setPropertiesContext,
      filterContext,
      getPrices,
    ]
  );

  const getProperties = useCallback(
    async (filterContext) => {
      try {
        const propertiesResponse = await fetchProperties(filterContext);

        if (filterContext.checkIn && filterContext.checkOut) {
          const propertiesResults = propertiesResponse.results;
          const ids = propertiesResults.map((property) => property.id);
          getPrices(
            ids,
            filterContext.checkIn,
            filterContext.checkOut,
            propertiesResults,
            propertiesResponse
          );
        }

        setPropertiesContext(propertiesResponse);
      } catch (error) {
        console.error(
          "Error on fetching homes, try to reload the page and try again.",
          error
        );
      }
    },
    [setPropertiesContext, getPrices]
  );

  useEffect(() => {
    (async () => {
      let filters = {
        where,
        who: who,
        sort,
        page: 1,
      };

      if (when) {
        const [checkIn, checkOut] = when.split("/");
        filters = { ...filters, checkIn, checkOut };
      }

      setFilterContext(filters);
      getProperties(filters);
      setPropertiesContext(null);

      return filters;
    })();
  }, [
    when,
    where,
    who,
    sort,
    setFilterContext,
    getProperties,
    setPropertiesContext,
  ]);

  const filterToggle = useCallback(() => {
    setIsOpen(!isOpen);
  }, [isOpen]);

  useEffect(() => {
    (async () => {
      if (!regionContext || !regionContext.length) {
        try {
          const regions = await fetchRegions();
          setRegionContext(regions);
        } catch (error) {
          console.error(
            "Error on fetching Regions, try to reload the page and try again."
          );
        }
      }
    })();
  }, [setRegionContext, regionContext]);

  const filtersCount = useMemo(() => {
    let count = 0;

    if (where) count++;
    if (when) count++;
    if (who) count++;

    return count;
  }, [where, when, who]);

  const clearFilters = useCallback(() => {
    setWhere("");
    setWhen("");
    setWho("");
    setSearchParams("");
  }, [setSearchParams]);

  return (
    <header className={cn(block, { [`${block}--open`]: isOpen })}>
      <div className={`container--large ${block}__content`}>
        <div className={`${block}__logo`}>
          <Link to="/">
            <div className={`${block}__logo-image`}></div>
          </Link>
        </div>
        <nav className={`${block}__nav`}>
          <Link
            className={`${block}__nav-item ${block}__nav-item--active`}
            to="/homes"
          >
            Find Homes
          </Link>
          <Link className={`${block}__nav-item`} to="/homes">
            Partners
          </Link>
          <Link className={`${block}__nav-item`} to="/homes">
            Company Retreats
          </Link>
          <Link className={`${block}__nav-item`} to="/homes">
            More
            <img
              alt=""
              className={`${block}__nav-item-icon`}
              src={chevronDownBlueIcon}
            />
          </Link>
        </nav>
        <div className={`${block}__login`}>
          <Button theme="link">Sign In</Button>
          <Button theme="dark-outline">Sign Up</Button>
        </div>
        <div className={`${block}__filters`}>
          <div className={`${block}__title-bar`}>
            <button
              className={`${block}__close-button`}
              onClick={filterToggle}
            ></button>
            <div>Filter homes</div>
            <button onClick={clearFilters} className={`${block}__clear-button`}>
              Clear All
            </button>
          </div>
          <div className={`${block}__filters-query fieldset`}>
            <div className={`${block}__filter-region field`}>
              <label className={`${block}__filter-label`}>Where</label>
              <select
                className={cn(`${block}__select`, {
                  [`${block}__select--light`]: !where,
                })}
                value={where}
                onChange={whereChangeHandler}
              >
                <option value="">Any region</option>
                {regionContext?.map((region) => (
                  <option value={region.id} key={region.id}>
                    {region.name}, {region.stateCode}
                  </option>
                ))}
              </select>
            </div>
            <div className={`${block}__filter-period field`}>
              <div className={`${block}__filter-period-inner`}>
                <label className={`${block}__filter-label`}>When</label>
                <select
                  className={cn(
                    `${block}__select`,
                    `${block}__filter-period-select`,
                    {
                      [`${block}__select--light`]: !when,
                    }
                  )}
                  value={when}
                  onChange={whenChangeHandler}
                >
                  <option value="">Select...</option>
                  <option value="2022-05-10/2022-05-15">
                    May 10 - May 15 (5 nights)
                  </option>
                  <option value="2022-05-15/2022-05-25">
                    May 15 - May 25 (10 nights)
                  </option>
                  <option value="2022-12-01/2022-12-31">
                    Dec 1 - Dec 31 (31 nights)
                  </option>
                </select>
              </div>
              <button
                className={`${block}__filter-toggle`}
                onClick={filterToggle}
              >
                {filtersCount > 0 && (
                  <span className={`${block}__filter-badge`}>
                    {filtersCount}
                  </span>
                )}
              </button>
            </div>
            <div className={`${block}__filter-guests field`}>
              <label className={`${block}__filter-label`}>Who</label>
              <select
                className={cn(`${block}__select`, {
                  [`${block}__select--light`]: !who,
                })}
                value={who}
                onChange={whoChangeHandler}
              >
                <option value="">Select...</option>
                {guestsOptions}
              </select>
            </div>
            <div className={`${block}__filter-sort-order field`}>
              <label className={`${block}__filter-label`}>Order</label>
              <select
                className={`${block}__select`}
                value={sort}
                onChange={sortChangeHandler}
              >
                <option value="RELEVANCE">Relevance</option>
                <option value="PRICE_ASC">Price: lowest first</option>
                <option value="PRICE_DESC">Price: highest first</option>
              </select>
            </div>
          </div>
          <div
            className={`${block}__fieldset-coupon fieldset`}
            title={
              when
                ? `Enter a promotional code to update prices`
                : `Select a period to use promotional codes`
            }
          >
            <div className={`${block}__filter-coupon field`}>
              <label className={`${block}__filter-label`}>Coupon</label>
              <input
                className={`${block}__input`}
                type="text"
                placeholder="Got a code?"
                value={coupon}
                onChange={couponChangeHandler}
                disabled={!when}
              />
            </div>
          </div>
          <div className={`${block}__apply-button-wrapper`}>
            <Button
              className={`${block}__apply-button`}
              theme="solid-green"
              onClick={filterToggle}
            >
              Apply
            </Button>
          </div>
        </div>
      </div>
    </header>
  );
};

export default MainHeader;
