import React, { useMemo } from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import "./property-card.scss";
import LocationSeparator from "../../images/location-separator.svg";
import BedroomIcon from "../../images/bedroom-icon.svg";
import BathroomIcon from "../../images/bathroom-icon.svg";
import PoolIcon from "../../images/pool-icon.svg";
import GuestIcon from "../../images/guest-icon.svg";

const block = "property-card";
const PropertyCard = ({ property, propertyIndex, propertyCount, last }) => {
  const isWorking = useMemo(() => {
    return !property.hasOwnProperty("id");
  }, [property]);

  const locationChunk = useMemo(() => {
    if (!property.regionName || !property.cityName || !property.stateCode)
      return null;

    return property.regionName !== property.cityName ? (
      <div className={`${block}__location-wrapper`}>
        {property.regionName}
        <img
          className={`${block}__location-separator`}
          alt=""
          src={LocationSeparator}
        />
        {property.cityName}, {property.stateCode}
      </div>
    ) : (
      <div className={`${block}__location-wrapper`}>
        {property.cityName}, {property.stateCode}
      </div>
    );
  }, [property.cityName, property.regionName, property.stateCode]);

  const amenitiesChunk = useMemo(() => {
    if (
      !property.bedsCount &&
      !property.bathroomsCount &&
      !property.hasPool &&
      !property.maxOccupancy
    )
      return null;

    const bedrooms = property.bedsCount ? (
      <>
        <img className={`${block}__amenity-icon`} src={BedroomIcon} alt="" />
        {property.bedsCount}&nbsp;Bedrooms
      </>
    ) : null;
    const bathrooms = property.bathroomsCount ? (
      <>
        <img className={`${block}__amenity-icon`} src={BathroomIcon} alt="" />
        {property.bathroomsCount}&nbsp;Bathrooms
      </>
    ) : null;
    const pool = property.hasPool ? (
      <>
        <img className={`${block}__amenity-icon`} src={PoolIcon} alt="" />
        Pool
      </>
    ) : null;
    const guests = property.maxOccupancy ? (
      <>
        <img className={`${block}__amenity-icon`} src={GuestIcon} alt="" />
        {property.maxOccupancy}&nbsp;Guests
      </>
    ) : null;

    return (
      <>
        {bedrooms}
        {bathrooms}
        {pool}
        {guests}
      </>
    );
  }, [
    property.bedsCount,
    property.bathroomsCount,
    property.hasPool,
    property.maxOccupancy,
  ]);

  const hasPrice = useMemo(() => {
    return property.hasPrice || !property.hasOwnProperty("id");
  }, [property]);

  return (
    <div
      className={cn(block, {
        [`${block}--working`]: isWorking,
        [`${block}--last`]: last,
      })}
    >
      <div
        className={`${block}__image-wrapper`}
        style={{
          backgroundImage: `url(${
            property && property.photos && property.photos[0].url
          })`,
        }}
      ></div>
      <div className={`${block}__info-wrapper`}>
        <div className={`${block}__info-inner-wrapper`}>
          <div className={`${block}__location`}>{locationChunk}</div>
          <div className={`${block}__title`}>{property.title}</div>
          <div className={`${block}__amenities`}>{amenitiesChunk}</div>
        </div>
        <div
          className={cn(`${block}__price-wrapper`, {
            [`${block}__price-wrapper--working`]:
              !property?.pricing && hasPrice,
            [`${block}__price-wrapper--show`]: property?.pricing && hasPrice,
          })}
        >
          {!property.hasPrice && (
            <>
              {property?.seasonPricing?.lowSeason && (
                <div className={`${block}__price-block`}>
                  <div className={`${block}__price-block-period`}>
                    <div className={`${block}__price-block-icon`}></div>
                    Low Season
                  </div>
                  <div className={`${block}__price-block-price`}>
                    {`$${property?.seasonPricing?.lowSeason?.minPrice} - $${property?.seasonPricing?.lowSeason?.maxPrice}`}
                  </div>
                  <div className={`${block}__price-block-details`}>
                    per night
                  </div>
                </div>
              )}

              {property?.seasonPricing?.highSeason && (
                <div className={`${block}__price-block`}>
                  <div className={`${block}__price-block-period`}>
                    <div
                      className={`${block}__price-block-icon ${block}__price-block-icon--high`}
                    ></div>
                    High Season
                  </div>
                  <div
                    className={`${block}__price-block-price`}
                  >{`$${property?.seasonPricing?.highSeason?.minPrice} - $${property?.seasonPricing?.highSeason?.maxPrice}`}</div>
                  <div className={`${block}__price-block-details`}>
                    per night
                  </div>
                </div>
              )}
            </>
          )}
          {hasPrice && (
            <div
              className={`${block}__price-block ${block}__price-block--working`}
            >
              <div
                className={cn(`${block}__price-block-period`, {
                  [`${block}__price-block-period--working`]:
                    !property?.pricing && hasPrice,
                  [`${block}__price-block-period--price`]: property?.pricing,
                })}
              >
                {property?.pricing ? (
                  <>
                    Total&nbsp;&nbsp;•&nbsp;&nbsp;
                    {property?.pricing?.numberOfNights}&nbsp;nights
                  </>
                ) : null}
              </div>
              <div
                className={cn(`${block}__price-block-price`, {
                  [`${block}__price-block-price--working`]:
                    !property?.pricing?.total,
                })}
              >
                {property.pricing ? `$${property?.pricing?.total}` : null}
              </div>
              <div
                className={cn(`${block}__price-block-details`, {
                  [`${block}__price-block-details--working`]:
                    !property?.pricing?.total,
                })}
              >
                {property.pricing
                  ? `$${
                      property?.pricing?.total /
                      property?.pricing?.numberOfNights
                    } per night`
                  : null}
              </div>
            </div>
          )}
        </div>
        <div className={`${block}__counter`}>
          <span className={`${block}__counter-highlight`}>{propertyIndex}</span>
          &nbsp;of&nbsp;
          <span className={`${block}__counter-highlight`}>{propertyCount}</span>
          &nbsp;homes in&nbsp;
          <span className={`${block}__counter-highlight`}>
            {property?.regionName}
          </span>
        </div>
      </div>
    </div>
  );
};

PropertyCard.propTypes = {
  home: PropTypes.any,
  last: PropTypes.bool,
};

export default PropertyCard;
