import PropTypes from "prop-types";
import React, {
  useMemo,
  useCallback,
  useContext,
  useState,
  useRef,
  useLayoutEffect,
} from "react";
import HomeCard from "../property-card/property-card";
import "./property-grid.scss";
import satelliteSvg from "../../images/satellite.svg";
import Button from "../button/button";
import {
  fetchProperties,
  fetchHomesPricing,
} from "../../services/property-service";
import { AppContext } from "../../App";
import { Link } from "react-router-dom";
import { useInView } from "react-intersection-observer";

const block = "property-grid";
const PropertyGrid = ({ numberOfSkeletonCards, properties, filters }) => {
  const rootRef = useRef(null);
  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const { propertiesContext, setPropertiesContext, filterContext } =
    useContext(AppContext);
  const { ref } = useInView({
    onChange: (inView) => {
      if (inView && !isLoadingMore) loadMoreHandler();
    },
    threshold: 0,
  });
  const dataSource = useMemo(() => {
    return (
      properties?.results ||
      [...Array(numberOfSkeletonCards).keys()].map(() => {
        return {};
      })
    );
  }, [properties?.results, numberOfSkeletonCards]);

  const loadMoreHandler = useCallback(() => {
    (async () => {
      setIsLoadingMore(true);

      try {
        const nextPage = properties.currentPage + 1;
        const propertiesResponse = await fetchProperties({
          ...filters,
          page: nextPage,
        });

        setPropertiesContext({
          ...propertiesContext,
          results: [
            ...propertiesContext.results,
            ...propertiesResponse.results,
          ],
          currentPage: propertiesResponse.currentPage,
        });

        setIsLoadingMore(false);

        if (filterContext.checkIn && filterContext.checkOut) {
          const propertiesResults = propertiesResponse.results;
          const ids = propertiesResults.map((property) => property.id);
          fetchHomesPricing(
            ids,
            filterContext.checkIn,
            filterContext.checkOut
          ).then((response) => {
            response.forEach((price) => {
              propertiesResults.forEach((property) => {
                if (price.homeId === property.id) {
                  property.pricing = price;
                }
              });
            });

            setPropertiesContext({
              ...propertiesContext,
              results: [...propertiesContext.results, ...propertiesResults],
              currentPage: propertiesResponse.currentPage,
            });
          });
        }
      } catch (error) {
        console.error(
          "Error on fetching homes, try to reload the page and try again.",
          error
        );
        setIsLoadingMore(false);
      }
    })();
  }, [
    properties,
    filters,
    setPropertiesContext,
    propertiesContext,
    filterContext.checkIn,
    filterContext.checkOut,
  ]);

  const propertyCount = useMemo(() => {
    return properties?.count;
  }, [properties?.count]);

  const hasMoreToLoad = useMemo(() => {
    return properties?.currentPage < properties?.totalPages;
  }, [properties?.currentPage, properties?.totalPages]);

  useLayoutEffect(() => {
    if (properties?.results?.length > 1 && properties?.currentPage === 1) {
      rootRef?.current?.scrollTo({
        top: 0,
        left: 100,
        behavior: "instant",
      });
      window.setTimeout(() => {
        rootRef?.current?.scrollTo({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
      }, 800);
    }
  }, [properties?.results, properties?.currentPage]);

  return (
    <div className={block} ref={rootRef}>
      {properties?.results && !properties.results.length ? (
        <div className={`${block}__empty`}>
          <img src={satelliteSvg} alt="Satellite" />
          <p className={`${block}__empty-text`}>
            Oops! We haven't found anything mathing your search.
            <br />
            Try something else or reset the filters to see all region homes.
          </p>
          <Link to="/">
            <Button
              className={`${block}__empty-state-cta`}
              theme="green-outline"
            >
              See all homes
            </Button>
          </Link>
        </div>
      ) : (
        <div className={`${block}__content`}>
          <div className={`${block}__count`}>
            <div className={`${block}__count-eyebrow`}>
              {propertyCount ? `your stay in one of` : `please wait`}
            </div>
            <div className={`${block}__count-title`}>
              {propertyCount && (
                <span className={`${block}__count-title--highlight`}>
                  {propertyCount}&nbsp;
                </span>
              )}
              {propertyCount ? (
                `homes`
              ) : (
                <>
                  <span className={`${block}__count-title--highlight`}>
                    Loading
                  </span>
                  &nbsp;homes
                </>
              )}
            </div>
          </div>
          {dataSource.map((home, homeIndex) => (
            <HomeCard
              property={home}
              propertyIndex={homeIndex + 1}
              propertyCount={propertyCount}
              last={homeIndex + 1 === dataSource.length && !hasMoreToLoad}
              key={`home-card-${homeIndex}`}
            />
          ))}
          {hasMoreToLoad && (
            <div className={`${block}__load-more-wrapper`}>
              <button
                ref={ref}
                className={`${block}__load-more-button`}
                onClick={loadMoreHandler}
                disabled={isLoadingMore}
              >
                {isLoadingMore ? `Loading more...` : `Load more`}
              </button>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

PropertyGrid.propTypes = {
  numberOfSkeletonCards: PropTypes.number,
  properties: PropTypes.shape({
    results: PropTypes.array,
    count: PropTypes.number,
  }),
  filters: PropTypes.object,
};

PropertyGrid.defaultProps = {
  numberOfSkeletonCards: 3,
};

export default PropertyGrid;
