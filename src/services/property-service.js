import { axiosInstance } from "./base-service";

/**
 * Calls remote homes API
 * @param  {string} where Region ID
 * @param  {Date} checkIn Check-in date in YYYY-MM-DD format
 * @param  {Date} checkOut Check-out date in YYYY-MM-DD format
 * @param  {string|number} who Number of guests
 * @param  {string} sort Query sort order
 * @param  {number} page Page number
 * @return {Object}
 */
const fetchProperties = ({
  where,
  checkIn,
  checkOut,
  who,
  sort,
  page,
} = {}) => {
  const graphqlQuery = {
    variables: {
      period: checkIn && checkOut ? { checkIn, checkOut } : undefined,
      region: where,
      guests: who ? parseInt(who) : undefined,
      order: sort,
      page,
      pageSize: 10,
    },
    query: `query FetchProperties(
      $region: UUID
      $period: BookingPeriod
      $guests: Int
      $order: HomesOrder!
      $page: Int!
      $pageSize: Int!
    ) {
      homes(
        region: $region
        period: $period
        guests: $guests
        order: $order
        page: $page
        pageSize: $pageSize
      ) {
        count
        results {
          id
          title
          description
          photos {
            listOrder
            url
          }
          roomsCount
          bathroomsCount
          bedsCount
          maxOccupancy
          hasPool
          amenities
          seasonPricing {
            highSeason {
              minPrice
              maxPrice
            }
            lowSeason {
              minPrice
              maxPrice
            }
          }
          regionName
          cityName
          stateName
          stateCode
        }
      }
    }
    `,
  };
  return axiosInstance({
    data: graphqlQuery,
  }).then((response) => {
    const homes = response?.data?.data?.homes;
    const totalPages = Math.ceil(homes?.count / 10);
    let properties = homes?.results || [];

    if (properties.length && checkIn && checkOut) {
      properties =
        properties.map((property) => {
          return { ...property, hasPrice: true };
        }) || [];
    }

    return {
      ...homes,
      totalPages,
      currentPage: page,
      results: properties,
    };
  });
};

/**
 * Calls remote homesPricing API
 * @param  {string[]} ids Array with regions IDs
 * @param  {Date} checkIn Check-in date in YYYY-MM-DD format
 * @param  {Date} checkOut Check-out date in YYYY-MM-DD format
 * @param  {string} coupon Promotional code
 * @return {Object[]}
 */
const fetchHomesPricing = (ids, checkIn, checkOut, coupon) => {
  const graphqlQuery = {
    variables: {
      ids,
      period: { checkIn: checkIn || "", checkOut: checkOut || "" },
      coupon,
    },
    query: `query FetchHomesPricing(
      $ids: [UUID]!
      $period: BookingPeriod!
      $coupon: String
    ) {
      homesPricing(ids: $ids, period: $period, coupon: $coupon) {
        homeId
        numberOfNights
        total
      }
    }
    
    `,
  };
  return axiosInstance({
    data: graphqlQuery,
  }).then((response) => response?.data?.data?.homesPricing || []);
};

export { fetchProperties, fetchHomesPricing };
