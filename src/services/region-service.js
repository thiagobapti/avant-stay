import { axiosInstance } from "./base-service";

/**
 * Calls remote regions API
 * @return {Object[]}
 */
const fetchRegions = () => {
  const graphqlQuery = {
    query: `{
          regions {
            id,
            name,
            stateName,
            stateCode
          }
        }`,
  };
  return axiosInstance({
    data: graphqlQuery,
  }).then((response) => response?.data?.data?.regions || []);
};

export { fetchRegions };
