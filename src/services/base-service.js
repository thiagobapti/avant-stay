import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://fake-api.avantstay.dev/graphql",
  method: "post",
  headers: {
    "content-type": "application/json",
  },
});

export { axiosInstance };
