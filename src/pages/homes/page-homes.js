import React, { useContext } from "react";
import "./page-homes.scss";
import MainHeader from "../../components/main-header/main-header";
import PropertyGrid from "../../components/property-grid/property-grid";
import { AppContext } from "../../App";

const block = "page-homes";
const PageHomes = () => {
  const { propertiesContext, filterContext } = useContext(AppContext);

  return (
    <div className={block}>
      <PropertyGrid properties={propertiesContext} filters={filterContext} />
      <MainHeader />
    </div>
  );
};

export default PageHomes;
