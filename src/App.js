import React, { createContext, useState, useEffect } from "react";
import "./App.scss";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import PageHomes from "./pages/homes/page-homes";
import { getLocalStorage, setLocalStorage } from "./utils/storage";

const AppContext = createContext();
const DataProvider = ({ children }) => {
  const [filterContext, setFilterContext] = useState(
    () => getLocalStorage("filterContext") || {}
  );

  const [regionContext, setRegionContext] = useState(
    () => getLocalStorage("regionContext") || []
  );

  const [propertiesContext, setPropertiesContext] = useState();

  useEffect(() => {
    setLocalStorage("filterContext", filterContext);
    setLocalStorage("regionContext", regionContext);
  }, [filterContext, regionContext]);

  const value = {
    filterContext,
    setFilterContext,
    regionContext,
    setRegionContext,
    propertiesContext,
    setPropertiesContext,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

function App() {
  return (
    <DataProvider>
      <Router>
        <Routes>
          <Route exact path="/" element={<Navigate to="/homes" />} />
          <Route exact path="/homes" element={<PageHomes />} />
        </Routes>
      </Router>
    </DataProvider>
  );
}

export default App;
export { DataProvider, AppContext };
