## Tech Explanation

[https://tinyurl.com/frontend-thiago-baptista](https://tinyurl.com/frontend-thiago-baptista)

## Live version

[https://avant-stay.vercel.app](https://avant-stay.vercel.app)

## Running Locally

`npm start`\
`npm run build`
